<?php

namespace Drupal\advanced_pwa_rules\Plugin\RulesAction;

use Drupal\rules\Core\RulesActionBase;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\advanced_pwa\Model\SubscriptionsDatastorage;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Url;

/**
 * Provides a 'Send user push notification' action.
 *
 * @RulesAction(
 *   id = "advanced_pwa_rules_send_user_push_notification",
 *   label = @Translation("Send user push notification"),
 *   category = @Translation("Advanced PWA rules"),
 *   context_definitions = {
 *     "uid" = @ContextDefinition("integer",
 *       label = @Translation("UID"),
 *       description = @Translation("Specifies the uid of the user to send the notification to.")
 *     ),
 *     "notification_title" = @ContextDefinition("text",
 *       label = @Translation("Notification title"),
 *       description = @Translation("Enter the notification title.")
 *     ),
 *     "notification_message" = @ContextDefinition("text",
 *       label = @Translation("Message"),
 *       description = @Translation("Enter the notification content.")
 *     ),
 *     "content_link" = @ContextDefinition("string",
 *       label = @Translation("Link"),
 *       description = @Translation("Enter the absolute URL to use in the notification ie https://example.com/news.")
 *     ),
 *   }
 * )
 */
class SendUserPushNotification extends RulesActionBase {

  /**
   * Sends the notification.
   */
  protected function doExecute($uid, $notification_title, $notification_message, $content_link) {
    
    // Check if push notifications are turned on globally
    $status = \Drupal::config('advanced_pwa.settings')->get('status.all');
    if ($status) {
      $advanced_pwa_config = \Drupal::config('advanced_pwa.advanced_pwa');
      $icon = $advanced_pwa_config->get('icon_path');
      $icon_path = \Drupal::service('file_url_generator')->generateAbsoluteString($icon);
      $entry = [
        'title' => $notification_title,
        'message' => $notification_message,
        'icon' => $icon_path,
      ];
      if ($content_link) {
          $entry['url'] = $content_link;
      }
      $notification_data = Json::encode($entry);
      $push_notification_service = \Drupal::service('advanced_pwa.push_notifications');
      $subscriptions = SubscriptionsDatastorage::loadAllByUID($uid);
      $advanced_pwa_public_key = $advanced_pwa_config->get('public_key');
      $advanced_pwa_private_key = $advanced_pwa_config->get('private_key');
      if (!empty($subscriptions) && !empty($advanced_pwa_public_key) && !empty($advanced_pwa_private_key)) {
        /** @var QueueFactory $queue_factory */
        $queue_factory = \Drupal::service('queue');
        $queue = $queue_factory->get('cron_send_notification');
        $item = new \stdClass();
        $item->subscriptions = $subscriptions;
        $item->notification_data = $notification_data;
        $queue->createItem($item);
      }
    }
  }
}
